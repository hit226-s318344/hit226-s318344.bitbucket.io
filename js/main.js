/**
* Scripts for {Project name and url}
*
* author: {name}
* email: {email}
* website: {portfolio}
*/
var temp="";
for (i = 1; i <= 10; i++) { temp += i.toString()+" "; }
var writeHere = document.getElementById("writeHere")
    writeHere.innerHTML = temp;


/**
 * Google AutoComplete API
 */

let autocomplete;
function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(document.getElementById('street'),
        {
            fields: ['address_components']
        }
    );
}